#!/bin/bash

servers=(
clnode057.clemson.cloudlab.us
#130.127.133.100
130.127.133.82
130.127.133.77
130.127.133.95
130.127.133.50
)
files=(
fclient1
fclient2
fclient3
fclient4
fclient5
)
client=128.104.222.116

slen=${#servers[@]}

cnt=11

while (( cnt > 0))
do
     cmd=`expr $RANDOM % 2`
     sindex=`expr $RANDOM % slen`     
     server=${servers[$sindex]}
     file=${files[$sindex]}
     
     echo $server
     echo $client
     echo $file
     if [[ $cmd == 0 ]];  then
          # scp
          #echo scp
          #time scp fuqianz@$server:/tmp/fdata/testdata /tmp/fdata/fclient
     else
          # grid ftp
          echo gridftp
          cnt=expr $cnt - 1
          echo $cnt 
	  time globus-url-copy -r -cd gsiftp://$server/tmp/fdata/testdata \
    		gsiftp://$client/tmp/fdata/$file
     fi

     sleep `expr $RANDOM % 120`
done 
