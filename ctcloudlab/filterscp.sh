#!/bin/bash

files=`ls /tmp/phase1-mixed`

sort $files

for file in $files
do
    echo $file
    tcpdump -r phase1-mixed/$file 'tcp and not port 2811 and src host c220g2-030631.wisc.cloudlab.us and dst host c220g2-030632.wisc.cloudlab.us or tcp and not port 2811 and src host c220g2-030631.wisc.cloudlab.us and dst host c220g2-030632.wisc.cloudlab.us' -w  scp$file
done
