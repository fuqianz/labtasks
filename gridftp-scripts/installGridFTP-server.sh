#!/bin/bash

mkdir GridFTP
cd GridFTP

# install wget
sudo apt-get -y install wget

# download globus
wget https://downloads.globus.org/toolkit/gt6/stable/installers/repo/deb/globus-toolkit-repo_latest_all.deb
 
# install globus
sudo dpkg -i  globus-toolkit-repo_latest_all.deb

# auto install dependency
sudo apt-get -y -f install 

# install globus
sudo dpkg -i  globus-toolkit-repo_latest_all.deb

# update resource 
sudo apt-get update

# install GridFTP GRAM5 globus-gsi
sudo apt-get install globus-gridftp globus-gram5 globus-gsi myproxy myproxy-server myproxy-admin
