#!/bin/bash
#------------------------------------------------------------------------------
#  Xen-tools is required. (http://xen-tools.org/software/xen-tools/)
#  Source code: https://gitlab.com/xen-tools/xen-tools 
#------------------------------------------------------------------------------

apt-get install -y perl perl-base \
        libconfig-inifiles-perl libtext-template-perl \
        libdata-validate-domain-perl libdata-validate-ip-perl \
        libdata-validate-uri-perl libfile-slurp-perl libfile-which-perl \
        libsort-versions-perl liblog-message-perl debootstrap

if [[ $? -ne 0 ]]; then
    echo ""
    echo "Error occurs during installing prerequests"
    echo ""
    exit 1
fi

tar -xf ./xen-tools-4.6.2.tar.gz
cd ./xen-tools-4.6.2
make clean
make uninstall
make install


