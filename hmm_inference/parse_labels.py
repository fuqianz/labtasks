#!/usr/bin/env python2
from scapy.all import *
import sys
import os
import datetime
import matplotlib.pyplot as plt

if __name__ == "__main__":
    if not len(sys.argv) == 3:
        print 'Usage: ./parse_labels.py delta-time mappings'
        sys.exit()

    delta_list = []

    with open(sys.argv[1],'r') as f:
        delta_list = f.read().strip().split('\n')

    delta_list = [float(x.strip()) for x in delta_list]


    with open(sys.argv[2],'r') as f:
        lines = f.read().strip().split('\n')

    ranges = []
    for l in lines:
        ranges += l.split(';')[0].split(',')

    ranges = [float(x) for x in ranges]
    ranges.sort()
    ranges.pop(0)
    ranges = list(set(ranges))
    ranges.sort()

    labels = []
    timings = {}
    for idx in range(len(ranges)):
        timings[chr(idx+97)] = []
    for t in delta_list:
        for idx,r in enumerate(ranges):
            if t <= r:
                labels.append(idx)
                timings[chr(idx+97)] += [t]
                break

    for l in timings.keys():
        tmp = timings[l]
        tmp.sort()
        str_tmp = [str(x) for x in tmp]
        with open('data/' + l, 'w') as f:
            f.write('\n'.join(str_tmp))


    chr_labels = [chr(97+x) for x in labels]
    ranges = [0] + ranges

    with open('data/labels.txt','w') as f:
        f.write(''.join(chr_labels))
