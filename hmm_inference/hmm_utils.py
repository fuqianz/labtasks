#!/usr/bin/env python2

## \package hmm_utils
#  \brief Tools needed to infer and utilize an HMM FSA.
#  \author Jon Oakley
#  \date 10/1/2017

import random
import pickle
import itertools
import math
from graphviz import Digraph
from scipy.stats import chisqprob
import sys
from hmm import *

## Load an HMM from a pickle file
#
# \param filename The pickle file
def load_hmm(filename):
    with open(filename, 'rb') as f:
        g = pickle.load(f)
    return g

# Not working for actual data
# May want to speed up merging function
def infer_hmm(label_file, l_max):
    l = 1
    l_test = 2
    equal = False
    while l_test < l_max:
        h1 = HMM()
        h1.infer(label_file,l)
        h2 = HMM()
        h2.infer(label_file,l_test)
        equal = hmm_equal(h1,h2,0.05)
        if equal:
            del h2
            return l,h1
        else:
            del h1
            del h2
            l = l_test
            l_test += 1

    return None,None

## Determine if two HMMs are equal by using the chi-squared metric
#
# \param hmm1 The first hmm to compare
# \param hmm2 The second hmm to compare
# \param alpha The alpha value to use for the chi-squared test
def hmm_equal(hmm1, hmm2, alpha):
    # Make h1 the hmm with the shorter window
    if hmm1.L > hmm2.L:
        h1 = hmm2
        h2 = hmm1
    else:
        h1 = hmm1
        h2 = hmm2

    # initialization
    fail = 0
    total = 0

    # Iterate through all the states in the shorter hmm
    for ks in h1.states.keys():
        total += 1
        # convert the current state in hmm1 to a list of possible
        # states in hmm2 based on the observed expressions in hmm2
        perms = [x + k for k in ks.split(',') for x in h2.expressions]

        # create a list of actual states in hmm2 based on all the 
        # possible states. Stores 'None' if the state isn't found
        h2_states = []
        for p in perms:
            s = h2.get_state(p)
            if not s == None:
                h2_states.append(s)

        # verifies that there is only one possible equivalent state in hmm2
        # otherwise an automatic fail
        if not len(set(h2_states)) == 1:
            fail += 1
        else:
            # Assuming there is a single equivalent state in hmm2, compare the probabilities
            # using the chi-squared test
            result = chi_square_test(h1.states[ks],list(set(h2_states))[0], alpha,'expression')
            if result == False:
                fail += 1

    # Overall, verify that the fail rate is less than the
    # alpha value
    if float(fail)/float(total) < alpha:
        return True
    else:
        return False

## Run a chi-squared test on two states
#
# \param s1 first state
# \param s2 second state
# \param alpha Alpha value to use
# \param method Either 'expression' or 'state'.  By 'expression' compares the probabilities of a 
# tranisitions. By 'state' compares the two states for equality
#
# Uses the formula $\sum_{r,c} \frac{(O_{r,c} - E_{r,c})^2}{E_{r,c}}$
# where E_{r,c} = \frac{n_r*n_c}{n}$
# and n_r is total number of occurrences of a state (s1 or s2),
# n_c is the total number occurences s1 and s2 transition to a 
# given state, and and n is the sum of the occurences of s1 and s2
def chi_square_test(s1,s2,alpha,method):
    # Get a list of the number of occurrences of outgoing transitions
    s1_next = s1.get_next_states()
    s1_occ = [s1.next_states[x][0] for x in s1_next]
    s1_exp = [s1.next_states[x][2] for x in s1_next]

    s2_next = s2.get_next_states()
    s2_occ = [s2.next_states[x][0] for x in s2_next]
    s2_exp = [s2.next_states[x][2] for x in s2_next]

    # Set the identification method
    if method == 'expression':
        s1_id = s1_exp
        s2_id = s2_exp
    elif method == 'state':
        s1_id = s1_next
        s2_id = s2_next

    # List of unique transitions or states
    sym_diff = list(set(s1_id) ^ set(s2_id))

    # Add missing transitions or states
    for s in sym_diff:
        if not s in s1_id:
            s1_id.append(s)
            s1_occ.append(0)
        elif not s in s2_id:
            s2_id.append(s)
            s2_occ.append(0)

    # Sort by the identification method
    s1_z = sorted(zip(s1_id,s1_occ))
    s2_z = sorted(zip(s2_id,s2_occ))

    # Calculate the $X^2$ statistic
    df = len(s1_z)-1

    nr1 = s1.total_occurences
    nr2 = s2.total_occurences
    n = nr1+nr2

    X2 = 0
    for idx in range(len(s1_z)):
        occ1 = s1_z[idx][1]
        occ2 = s2_z[idx][1]
        nc = occ1 + occ2
        E1 = float(nr1*nc)/float(n)
        E2 = float(nr2*nc)/float(n)
        X2 += (pow(occ1-E1,2)/E1) + (pow(occ2-E2,2)/E2)

    p = chisqprob(X2,df)

    # Accept the null hypothesis
    if p > alpha or X2 == 0:
        return True
    # Reject the null hypothesis
    else:
        return False

if __name__ == "__main__":
    L,h1 = infer_hmm('data/labels-pmu.txt',6)
    if not L == None:
        print "HMM inferred with L=%d" % L
        h1.save_hmm('output/hmm.object')
        h1.print_dot_graph('pmu')

    #g = load_graph('output/hmm.object')
    #g.load_hmm('output/')
    #for idx in range(100):
    #    print `g.get_observation()`
