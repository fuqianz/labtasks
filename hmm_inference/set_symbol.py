#!/usr/bin/env python2
from scapy.all import *
import sys
import os
import datetime
import matplotlib.pyplot as plt 
import math, time, pylab; 
import numpy as np; 

if __name__ == "__main__":
    if not len(sys.argv) == 2:
        print 'Usage: ./assign_labels.py delta-time'
        sys.exit()
    delta_list = []

    with open(sys.argv[1],'r') as f:
        delta_list = f.read().strip().split('\n')

    delta_list = [float(x.strip()) for x in delta_list]


    freq,bins = np.histogram(delta_list,10000)
    fig = plt.figure(dpi=100); 
    freq = np.append(freq,[0.0])
    graph = fig.add_subplot(111);
    x = graph.bar(bins, freq)


    plt.title("Interpacket Delay Histogram")
    plt.xlabel("Time")
    plt.ylabel("Frequency")
    fig.savefig(sys.argv[1]+'.pdf')
    plt.show()
