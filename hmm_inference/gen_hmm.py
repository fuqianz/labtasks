#!/usr/bin/env python2

## \package hmm_utils
#  \brief Tools needed to infer and utilize an HMM FSA.
#  \author Jon Oakley
#  \date 10/1/2017

import random
import pickle
import itertools
import math
from graphviz import Digraph
from scipy.stats import chisqprob
import sys
from hmm import *

## Load an HMM from a pickle file
#
# \param filename The pickle file
def load_hmm(filename):
    with open(filename, 'rb') as f:
        g = pickle.load(f)
    return g

if __name__ == "__main__":
    h1 = HMM()
    h1.infer_hmm('data/labels.txt',L=2)
    h1.save_hmm('output/hmm.object')
    h1.print_dot_graph('output/pmu')

    #g = load_graph('output/hmm.object')
    #g.load_hmm('output/')
    #for idx in range(100):
    #    print `g.get_observation()`
