# HMM Inference Algorithm

1. get_delta.py:
INPUT: PCAP File
OUTPUT: Delta Times
Takes input PCAP files and generates a txt file of delta times.

2. assign_labels.py:
INPUT: Delta Times
OUTPUT: Mappings
Takes input delta time file (TXT) and generates a histogram plot.  Lets user
set the label boundaries. Generates a mapping file.

3. parse_labels.py:
INPUT: Mappings, Delta Times
OUTPUT: observations, labels.txt
Using the mappings to create a string of labels and groups the observations
into their associated label files.

4. gen_hmm.py:
INPUT: labels.txt
OUTPUT: dot file, pdf file, object file
Creates an HMM object from the label string.
