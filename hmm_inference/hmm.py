#!/usr/bin/env python2

## \package hmm
#  \brief Everything needed to utilize an HMM FSA.
#  \author Jon Oakley
#  \date 06/22/2017
#
#  Read in data into an HMM, advance through states, and generate data
#  from the HMM.
#
#  The term `Expression' or `expr' is used to reference the output of a state transition.
# The need for this arises from the fact that states are collapsed and output can no 
# longer be determined by the last letter of a given state

import random
import pickle
import itertools
import math
from graphviz import Digraph
from scipy.stats import chisqprob
import sys
from hmm_utils import *

class HMM():
    def __init__(self):
        self.hmm_symbols = ''
        self.L = 1
        self.collapse = True
        self.current_state = None
        self.states = {}
        self.alpha = 0.05
        self.observations = {}
        self.expressions = []

    ## Reset the graph for re-inferencing
    def reset(self):
        self.expressions = []
        self.current_state = None
        # Delete old states
        for k in self.states.keys():
            del self.states[k]
        self.states = {}
        self.alpha = 0.05


    ## Infer the HMM from a symbol file
    #
    # \param data_file The file that contains the string
    # \param L The length of the window to use
    # \param merged A dictionary of state subsitutions (when states are deamed equal)
    # \param Alpha The confidence value to use for state collapsing
    #
    # \bug May not work for multiple state substitutions ('aa,bb' -> 'aa,bb,cc')
    def infer(self, data_file, L, merged={}, alpha=.05,collapse=True):
        self.hmm_symbols = data_file
        self.L = L
        self.alpha = alpha
        self.collapse = collapse
        with open(self.hmm_symbols,'r') as f:
            for item in f.read().strip().split('\n'):
                self.add_observation(item,L,merged)

        self.print_txt_graph()
        if self.collapse:
            self.collapse_equal_states(merged)

    ## Use an observation to adjust the HMM
    #
    # \param item The observation
    # \param L The window length
    # \param merged The dictionary of state substitutions
    def add_observation(self, item, L, merged):
        # Break up the input sequence into chunks of length L
        str_states = [item[i:i+L] for i in range(0,len(item)-L)]
        for s in str_states:
            # Substitute each state if applicable
            sub = s
            expr = s[-1]
            while sub in merged.keys():
                sub = merged[sub]

            # Add the occurence of the state
            self.update_state(sub,expr)

        # This state doesn't actually occur since
        # since it's the state after the last letter
        self.current_state.decrement_occurences()

    ## Incrementing the number of occurences for a given state
    #
    # \param state The state to increment
    # \param expr The label expressed between states
    def update_state(self, state, expr):
        if not expr in self.expressions:
            self.expressions.append(expr)

        # Check for start condition or new state
        # condition
        if self.current_state == None:
            self.states[state] = State(state)
            self.current_state = self.states[state]
            # Increment the number of occurences of the starting letter
            self.current_state.increment_occurences()
        else:
            next_state_keys = self.current_state.get_next_states()

            # Increment the occurences of the next state
            if state in next_state_keys:
                occ,s,expr = self.current_state.next_states[state]
                s.increment_occurences()
                self.current_state.next_states[state] = (occ+1,s,expr)
                self.current_state = s
                return
            elif not state in self.states.keys():
                # Create a new state
                self.states[state] = State(state)

            # Increment the overall occurences
            self.states[state].increment_occurences()
            # Create a new link to the new state
            self.current_state.next_states[state] = (1,self.states[state],expr)
            # Set the current state
            self.current_state = self.states[state]

    ## Merge all the equal states
    #
    # \param merged The dictionary of state substitutions
    def collapse_equal_states(self, merged):
        # Check every combination of states
        for s1,s2 in itertools.combinations(self.states.keys(),2):
            # Check to see if the states have the same distribution
            #if self.check_distribution(self.states[s1],self.states[s2]):
            if chi_square_test(self.states[s1],self.states[s2],self.alpha,'state'):
                print 'Merge: ' + s1 + ' and ' + s2
                # Create a new dictionary entry
                s_new= s1+','+s2
                merged[s1] = s_new
                merged[s2] = s_new
                # Reset the graph an re-infer using the new state substitutions
                self.reset()
                self.infer(self.hmm_symbols, self.L, merged, self.alpha)
                break

    ## Setup the HMM for proxy use
    #
    # Loads the HMM from a pickle file
    #
    # \param hmm_folder The directory containing the HMM files
    def import_observations(self, hmm_folder):
        # Choose a random state
        self.current_state = self.states[random.choice(self.states.keys())]

        # Assumes observation file exists for each expression in HMM
        for expr in self.expressions:
            self.observations[expr] = []
            with open(hmm_folder + '/' + expr,'r') as f:
                for line in f.readlines():
                    self.observations[expr].append(float(line.strip()))

    def get_observation(self):
        self.current_state,expr = self.current_state.random_state()
        return random.choice(self.observations[expr])

    def set_random_state(self):
        self.current_state = random.choice(self.states.values())

    def get_state(self, state):
        for k in self.states.keys():
            if state in k:
                return self.states[k]
        return None

    def save_hmm(self, savefile):
        with open(savefile, 'wb') as f:
            pickle.dump(self,f)

    def print_txt_graph(self):
        for cs in self.states.keys():
            print "State: " + cs + " | " + `self.states[cs].total_occurences`
            for ns in self.states[cs].get_next_states():
                print " --> " + ns + " : " + `self.states[cs].get_prob(ns)`

    def print_dot_graph(self,name):
        dot = Digraph(comment='HMM',format='pdf')
        for k in self.states.keys():
            dot.node(k)
        for node in self.states.values():
            for next_node in node.get_next_states():
                occ,s,expr = node.next_states[next_node]
                lab = expr + '(' + str(round(node.get_prob(next_node),3)) + ')'
                #lab = expr + '(' + str(occ) + ')'
                dot.edge(node.name, next_node, label=lab)

        dot.render(name)


class State():
    def __init__(self, name):
        ## Name of the state
        self.name = name
        ## Total number of times the state occurs
        self.total_occurences = 0
        ## Tuple (occ, <state>, expr)
        self.next_states = {}

    def get_next_states(self):
        return self.next_states.keys()

    def get_prob(self, key):
        occ,s,expr = self.next_states[key]
        return float(occ)/float(self.total_occurences)

    def increment_occurences(self):
        self.total_occurences += 1

    def decrement_occurences(self):
        self.total_occurences -= 1

    ## Advance the HMM and get the associated timing
    #
    # \returns A timing value
    #
    # Dartboard approach to choosing a next state.  Create a probability list:
    #
    # [0 ... 0.X ... 0.Y ... 1]
    #
    # Choose a random value: V
    #
    # [0 ... 0.X .. V .. 0.Y ... 1]
    #
    # Choose state associated with probability 0.X
    def random_state(self):
        if self.next_states.keys() == []:
            return None
        # A list of transition probabilities
        prob_range = [0]
        # A list of choices
        choices = []
        # A list of next states
        n_state = []
        # Populate the transition probabilities, choices, and next states
        for k,s in self.next_states.iteritems():
         prob_range.append(self.get_prob(k) + prob_range[-1])
         choices.append(k)
        prob_range[-1] = 1

        val = random.random()
        for idx in range(0,len(prob_range)):
         if prob_range[idx] <= val <= prob_range[idx + 1]:
             choose = choices[idx]
             return self.next_states[choose][1],self.next_states[choose][2]
